#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    char buf[BUFSIZ];
    int n ;

    while ( (n = read(STDIN_FILENO , buf , BUFSIZ)) > 0 ) {
        write( STDOUT_FILENO , buf , n );
    }
    return 0;
}

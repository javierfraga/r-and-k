#include <stdio.h>

int main(int argc, char *argv[])
{
    /*
     * # https://en.wikipedia.org/wiki/Printf_format_string
     * %[parameter][flags][width][.precision][length]type
     */
    char *string = (char *)"hello, world";
    printf("String\n");
    printf("%%s\t\t");
    printf(":%s:\n", string);

    printf("%%2s\t\t");
    printf(":%2s:\n", string);

    printf("%%.10s\t\t");
    printf(":%.10s:\n", string);

    printf("%%-2s\t\t");
    printf(":%-2s:\n", string);

    printf("%%-s\t\t");
    printf(":%-s:\n", string);

    printf("%%.11s\t\t");
    printf(":%.11s:\n", string);

    printf("%%-15s\t\t");
    printf(":%-15s:\n", string);

    printf("%%15.10s\t\t");
    printf(":%15.10s:\n", string);

    printf("%%-15.10s\t");
    printf(":%-15.10s:\n", string);

    printf("\n");
    printf("\n");

    int integer = 123456789;
    printf("Integer\n");
    printf("%%d\t\t");
    printf(":%d:\n", integer);

    printf("%%2d\t\t");
    printf(":%2d:\n", integer);

    printf("%%.10d\t\t");
    printf(":%.10d:\n", integer);

    printf("%%-2d\t\t");
    printf(":%-2d:\n", integer);

    printf("%%-d\t\t");
    printf(":%-d:\n", integer);

    printf("%%.11d\t\t");
    printf(":%.11d:\n", integer);

    printf("%%-15d\t\t");
    printf(":%-15d:\n", integer);

    printf("%%15.10d\t\t");
    printf(":%15.10d:\n", integer);

    printf("%%-15.10d\t");
    printf(":%-15.10d:\n", integer);


    printf("\n");
    printf("\n");


    double decimal = 123456.123456;
    printf("Float\n");
    printf("%%f\t\t");
    printf(":%f:\n", decimal);

    printf("%%2f\t\t");
    printf(":%2f:\n", decimal);

    printf("%%.10f\t\t");
    printf(":%.10f:\n", decimal);

    printf("%%-2f\t\t");
    printf(":%-2f:\n", decimal);

    printf("%%-f\t\t");
    printf(":%-f:\n", decimal);

    printf("%%.11f\t\t");
    printf(":%.11f:\n", decimal);

    printf("%%-15f\t\t");
    printf(":%-15f:\n", decimal);

    printf("%%15f\t\t");
    printf(":%15f:\n", decimal);

    printf("%%15.10f\t\t");
    printf(":%15.10f:\n", decimal);

    printf("%%-15.10f\t");
    printf(":%-15.10f:\n", decimal);
    return 0;
}

#include <stdio.h>
#include <stdlib.h>

void filecopy( FILE *ifp , FILE *ofp )
{
    int c;
    /*
     *Regular Standard Library functions
     */
    while ( (c = getc(ifp)) != EOF ) {
        putc( c , ofp );
    }

    /*
     *Shorthand Standard Library functions for stdio
     * only works when no file is passes to function
     */
    /*while ( (c = getchar()) != EOF ) {*/
        /*putchar(c);*/
    /*}*/
}

int main(int argc, char *argv[])
{
    FILE *fp;

    if ( argc == 1 ) {
        filecopy( stdin , stdout );
    } else {
        while ( --argc > 0 ) {
            if ( (fp = fopen( *++argv , "r" )) == NULL ) {
                fprintf(stderr , "cat: can't open %s\n", *argv);
                return( EXIT_FAILURE );
            } else {
                filecopy( fp , stdout );
                fclose( fp );
            }
        }
    }
    if ( ferror(stdout) ) {
        fprintf(stderr, "%s:error writing to stdout\n", *argv);
        exit(2);
    }
    return EXIT_SUCCESS;
}

#include <stdio.h>
#include <pthread.h>

#define NUM_THREADS 4

void foo()
{
    int a = 10;
    static int sa = 10;

    a += 5;
    sa += 5;

    printf("a = %d, sa = %d\n", a, sa);
}

void *foo_threads(void *pArg)
{
    /*
     *This will NOT reset static var
     */
    /*int a = 10;*/
    /*static int sa = 10;*/

    int myNum = *((int*)pArg);

    int i;
    for (i = 0; i < 3; ++i) {
        /*
         *This will reset static var
         */
        int a = 10;
        static int sa = 10;

        a += 5;
        sa += 5;

        printf("thread:%d, a = %d , sa = %d\n", myNum , a , sa);
    }
    return 0;
}

int main(int argc, char *argv[])
{
    int i;
    int tNum[NUM_THREADS];
    pthread_t tid[NUM_THREADS];

    for (i = 0; i < 12; ++i) {
        foo();
    }
    printf("\n");
    for(i = 0; i < NUM_THREADS; i++) { /* create/fork threads */
        tNum[i] = i;
        pthread_create(&tid[i], NULL, foo_threads, &tNum[i]);
    }

    for(i = 0; i < NUM_THREADS; i++) { /* wait/join threads */
        pthread_join(tid[i], NULL);
    }
    return 0;
}

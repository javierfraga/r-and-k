#include <stdio.h>

/*
 * Exercise 1-13. Write a program to print a histogram of the lengths of words in its input. It is
 * easy to draw the histogram with the bars horizontal; a vertical orientation is more challenging.
 */

int main(int argc, char *argv[])
{
    int character , count_character;
    character = count_character = 0;

    /*
     *horizontal historgram
     */
    /*while ( (character = getchar()) != EOF ) {*/
        /*if ( character == '\n' || character == ' ' || character == '\t' ) {*/
            /*printf(":\t\t");*/
            /*int i;*/
            /*for (i = 0; i < count_character; ++i) {*/
                /*printf("-");*/
            /*}*/
            /*printf("\n");*/
            /*count_character = 0;*/
        /*} else {*/
            /*putchar( character );*/
            /*count_character++;*/
        /*}*/
    /*}*/

    /*
     *vertical histogram
     */
    int count_words_length[10] = {0};
    int max_value = 0;
    while ( (character = getchar()) != EOF ) {
        if ( character == '\n' || character == ' ' || character == '\t' ) {
            if ( count_character >= 10 ) {
                count_words_length[0]++;
            } else {
                count_words_length[count_character]++;
            }
        } else {
            count_character++;
        }
    }
    int i;
    for (i = 0; i < 10; ++i) {
        if ( count_words_length[i] > max_value ) {
            max_value = count_words_length[i];
        }
    }
    for (i = max_value; i >= 0; i--) {
        printf("%d  | ", i);
        /*for (j = 0; j < 10; j++) {*/
            /*if ( j == 0 ) {*/
                
            /*} else {*/
                
            /*}*/
        /*}*/
    }
    return 0;
}

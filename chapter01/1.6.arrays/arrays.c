#include <stdio.h>

int main(int argc, char *argv[])
{
    int character , number_white_space , number_other , i;
    character = number_white_space = number_other = i =  0;
    int digits[10] = { 0 };

    while ( (character = getchar()) != EOF ) {
        if ( character >= '0' && character <= '9' ) {
            /*
             * getchar() only returns chars 
             * hex rep of '0' is 48 and '1' 49, etc.
             * which is why character = '0' works below
             */
            digits[ character - '0' ]++;
        } else if ( character == ' ' || character == '\t' || character == '\n' ) {
            number_white_space++;
        } else {
            number_other++;
        }
    }
    printf("here are the digits: \n");
    for (i = 0; i < 10; ++i) {
        printf("%d:%d \n", i , digits[i]);
    }
    printf("number of white spaces: %d \n", number_white_space);
    printf("number of anything else: %d \n", number_other);

    return 0;
}

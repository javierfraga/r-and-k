#include <stdio.h>

#define LOWER   0     /* lower limit of table */
#define UPPER   300     /* lower limit of table */
#define STEP    20     /* lower limit of table */

int main(int argc, char *argv[])
{
    int fahr;

    printf("Fahrenheit\tCelsius\n");
    for (fahr = LOWER; fahr < UPPER; fahr = fahr + STEP) {
        printf("%3d\t%6.1f\n", fahr , ( 5.0/9.0 )*( fahr - 32 ));
    }
    return 0;
}

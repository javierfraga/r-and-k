#include <stdio.h>

int main(int argc, char *argv[])
{
    int character;

    while ( (character = getchar()) != EOF ) {
        if ( character == ' ' ) {
            while ( (character = getchar()) == ' ' ) {
                ;
            }
            putchar(' ');
            if ( character == EOF ) {
                break;
            }
        }
        putchar( character );
    }

    return 0;
}

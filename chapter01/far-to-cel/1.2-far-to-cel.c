#include <stdio.h>

/*print Fahrenheit-Celcuis table*/
/*for farh = 0 , 20 , ... , 200*/

int main()
{
    /*
     *int version
     */
    /*int fahr, celcuis;*/
    /*int lower , upper , step;*/

    /*lower = 0;*/
    /*upper = 300;*/
    /*step = 20;*/

    /*fahr = lower;*/
    /*while ( fahr <= upper ) {*/
        /*celcuis = 5 * ( fahr - 32 ) / 9;*/
        /*printf("%d\t%d\n", fahr , celcuis);*/
        /*fahr = fahr + step;*/
    /*}*/

    /*
     *float version
     */
    float fahr, celcuis;
    float lower , upper , step;

    lower = 0;
    upper = 300;
    step = 20;

    fahr = lower;
    printf("Celsius\tFahrenheit\n");
    while ( fahr <= upper ) {
        celcuis = 5.0 * ( fahr - 32.0 ) / 9.0;
        printf("%g\t%5.1f\n", fahr , celcuis);
        fahr = fahr + step;
    }

    return 0;
}

#include <stdio.h>

int main(int argc, char *argv[])
{
    int character;
    while ( (character = getchar()) != EOF ) {
        if ( character == '\n' || character == ' ' || character == '\t' ) {
            putchar('\n');
        } else {
            putchar( character );
        }
    }
    return 0;
}

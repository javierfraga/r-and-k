#include <stdio.h>

#define IN      1 /*inside a word*/
#define OUT     0 /*outside a word*/

int main(int argc, char *argv[])
{
    int character , countLine , countWord , countChar , state;

    state = OUT;
    countChar = countLine = countWord = 0;
    while ( (character = getchar()) != EOF ) {
        ++countChar;
        if ( character == '\n' ) {
            countLine++;
        }
        if ( character == ' ' || character == '\n' || character == '\t' ) {
            state = OUT;
        } else if( state == OUT ){
            state = IN;
            countWord++;
        }
    }
    printf("line count: %d, word count: %d, character count: %d\n", countLine , countWord , countChar);
    return 0;
}
